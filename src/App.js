import React from "react";
import './App.css'

function App() {
    const ZnackiZodiack = [
        {
            id: 1,
            name: 'Козерог:',
            src: 'https://upload.wikimedia.org/wikipedia/commons/c/c1/RR5110-0045R.gif',
            text: "Козеро́г (лат. Capricornus) — десятый знак зодиака, соответствующий сектору эклиптики от 270° до 300°, считая от точки весеннего равноденствия; кардинальный знак тригона Земля."
        },
        {
            id: 2,
            name: 'Овен:',
            src: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTiOPgqM8LyEhqq4kJwAnqyAKHZIZwNnsczng&usqp=CAU',
            text: 'О́ве́н (лат. Aries, баран) — первый знак зодиака, соответствующий сектору эклиптики от 0° до 30°, считая от точки весеннего равноденствия; кардинальный знак тригона (трёх знаков) «огонь».'
        },
        {
            id: 3,
            name: 'Телец:',
            src: 'https://global27.ru/assets/images/sign/sign-Taurus.jpg',
            text: "Taurus) — второй знак зодиака, соответствующий сектору эклиптики от 30° до 60°, считая от точки весеннего равноденствия, и созвездию Телец; постоянный знак тригона Земля."
        },
        {
          id: 4,
          name: 'Скорпион:',
          src:"https://upload.wikimedia.org/wikipedia/commons/8/8a/RR5110-0043R.gif",
          text: 'Скорпион (лат. Scorpius) — восьмой знак зодиака, соответствующий сектору эклиптики от 210° до 240°, считая от точки весеннего равноденствия; постоянный знак тригона Вода.'
        },
        {
          id: 5,
          name: 'Стрелец:',
          src: 'https://upload.wikimedia.org/wikipedia/commons/b/b2/RR5111-0119R.gif',
          text: 'Стреле́ц (лат. Sagittarius) — девятый знак зодиака, соответствующий сектору эклиптики от 240° до 270°, считая от точки весеннего равноденствия; мутабельный (меняющийся) знак тригона (трёх знаков) «огня».'
        },
        {
          id: 6,
          name: 'Водолей:',
          src: 'https://upload.wikimedia.org/wikipedia/commons/4/44/RR5111-0122R.gif',
          text: 'Aquarius) — одиннадцатый знак зодиака, соответствующий сектору эклиптики от 300° до 330°, считая от точки весеннего равноденствия; постоянный знак тригона Воздух.'
        }
    ]

    const block = ZnackiZodiack.map((el)=>{
        return(
          <div key={el.id} className="style-div">
              <p className="style-p">{el.name}</p>
              <p className="style-text">{el.text}</p>
              <img src={el.src}/>
          </div>
        )
    })
    return(
        <>
            <p className="aling">Знаки зодиака</p>
            <div className="style-block">
                {block} 
            </div>
        </>
    )
}
export default App;
